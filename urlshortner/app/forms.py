from django import forms
from .widgets import JSONEditorWidget
from django.apps import apps
from app.models import JsonEditorModel
import json
from django.forms import CharField
from .widgets import ChainSelectJSONEditorWidget
from app.jsonconstant import SCHEMA_DICT

class PlatformMultiChoiceField(CharField):
    
    def __init__(self,chain_field, initial=None, *args, **kwargs):
        defaults={  
            'widget':ChainSelectJSONEditorWidget(SCHEMA_DICT,chain_field)
        }
        defaults.update(kwargs)
        print(chain_field)
        super(PlatformMultiChoiceField, self).__init__(initial=initial, *args,**defaults)
    
    class Media:
        js = ('js/jsoneditor.min.js',)
class JsonEditorForm(forms.ModelForm):

    jsondata=PlatformMultiChoiceField("platform")

    class Media:
        js = ('js/jsoneditor.min.js',)

     


        


        
        
  