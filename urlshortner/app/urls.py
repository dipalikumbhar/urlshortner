from django.contrib import admin
from django.urls import path,re_path
from app import views


urlpatterns = [
  path('', views.index, name = 'index'),
  path('search',views.search, name = 'search'),
  path('message',views.msg,name='msg'),
  path('jsoneditor',views.jsoneditor,name='jsoneditor'),
  
  # re_path(r'^\w+',views.search_view, name = 'search_view')
]
