from django.contrib import admin
from app.models import Info, JsonEditorModel
from .forms import JsonEditorForm


class InfoAdmin(admin.ModelAdmin):
    list_display = ("url", "shortnerurl",)


class JsonEditorModelAdmin(admin.ModelAdmin):
    form = JsonEditorForm
    list_display = ('platform', 'jsondata', )


admin.site.register(Info, InfoAdmin)

admin.site.register(JsonEditorModel, JsonEditorModelAdmin)
