# Generated by Django 3.0.4 on 2021-02-15 09:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20210215_1504'),
    ]

    operations = [
        migrations.RenameField(
            model_name='info',
            old_name='id',
            new_name='info_id',
        ),
    ]
