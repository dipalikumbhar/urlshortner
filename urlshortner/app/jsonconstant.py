SCHEMA_DICT = {
    "default": {
        "schema": {
            'type': 'object',
            'properties': {
                'authorization_key': {
                    'title': 'Auth Key',
                    'type': 'string',
                    'format': 'text',
                    },
                'conversion_value': {
                    'title': 'Conversion Value',
                    'type': 'integer',
                    'format': 'number',
                    }
                }
            }
        },
    "facebook": {
        "schema": {
            'type': 'object',
            'properties': {
                "access_token": {
                    "type": "string",
                    "default": "fgbgffgh"
                    },
                "content_name": {
                    "type": "string",
                    "default": "ddrvtr"
                    }
                }
            }
        },
    "custompixel": {
        "schema": {
            'type': 'object',
            'properties': {
                "conversion_value": {
                    "type": "integer",
                    "default": "0"
                    },
                "client_id": {
                    "type": "string",
                    "default": "ddrfgvtr"
                    },
                "client_secret": {
                    "type": "string",
                    "default": "dcssed"
                    }
                }
            }
        },
    "gdnpixel": {
        "schema": {
            'type': 'object',
            'properties': {
                'authorization_key': {
                    'title': 'Auth Key',
                    'type': 'string',
                    'format': 'text',
                    },
                'conversion_value': {
                    'title': 'Conversion Value',
                    'type': 'integer',
                    'format': 'number',
                    }
                }
            }
        },
    "mediago": {
        "schema": {
            'type': 'object',
            'properties': {
                'authorization_key': {
                    'title': 'Auth Key',
                    'type': 'string',
                    'format': 'text',
                    },
                'conversion_value': {
                    'title': 'Conversion Value',
                    'type': 'integer',
                    'format': 'number',
                    }
                }
            }
        },
    "outbrain": {
        "schema": {
            'type': 'object',
            }
        },
    "revcontent": {
        "schema": {
            'type': 'object',
            }
        },
    "taboola": {
        "schema": {
            'type': 'object',
            }
        },
    "yahoogemini": {
        "schema": {
            'type': 'object',
            'properties': {
                "client_id": {
                    "type": "string",
                    "default": "jhhr"
                    },
                "client_secret": {
                    "type": "string",
                    "default": "kluku"
                    },
                "dp": {
                    "type": "string",
                    "default": "branch"
                    }
                }
            }
        }
    }
