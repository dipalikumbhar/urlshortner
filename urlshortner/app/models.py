from django.db import models


class Info(models.Model):
    info_id = models.AutoField(primary_key=True)
    url = models.URLField(max_length=122)
    shortnerurl = models.URLField(max_length=122, blank=True)

    class Meta:
        ordering = ("url", "shortnerurl")

    def __str__(self):
        return self.url


PLATFORM_CHOICES = (
      ("default", "Default"),
      ("facebook", "Facebook"),
      ("custompixel", "CustomPixel"),
      ("gdnpixel", "GDNPixel"),
      ("mediago", "MediaGo"),
      ("outbrain", "Outbrain"),
      ("revcontent", "RevContent"),
      ("taboola", "Taboola"),
      ("yahoogemini", "YahooGemini"),
)


class JsonEditorModel(models.Model):
    platform = models.CharField(max_length=200, choices=PLATFORM_CHOICES,
                                default="default")
    jsondata = models.CharField(max_length=2000)
 