from django.utils.safestring import mark_safe
from django.forms import widgets


class JSONEditorWidget(widgets.Textarea):
    template_name = "jsoneditor.html"
    DISABLE_COLLAPSE = "true"
    DISABLE_EDITJSON = "true"

    def __init__(self, schema, editor_options=None):
        super(JSONEditorWidget, self).__init__()
        editor_options = {
            "theme": "bootstrap4",
            'disable_collapse': self.DISABLE_COLLAPSE,
            'disable_edit_json': self.DISABLE_EDITJSON,
            'form_name_root': " "
        }
        self.schema = schema
        self.editor_options = editor_options
        self.editor_options.update(editor_options or {})

    def render(self, name, value, attrs=None, renderer=None):
        schema = self.schema
        editor_options = self.editor_options.copy()
        editor_options['schema'] = schema
        context = {
            "name": name,
            "value": value,
            "options": editor_options
        }
        return mark_safe(renderer.render(self.template_name, context))


class ChainSelectJSONEditorWidget(JSONEditorWidget):
    template_name = "chainjsoneditor.html"

    def __init__(self, schema, chain_field):
        print(chain_field, "schema in chainfield")
        super(ChainSelectJSONEditorWidget, self).__init__(schema, chain_field)
        self.chain_field = chain_field

    def render(self, name, value, attrs=None, renderer=None):
        schema = self.schema
        editor_options = self.editor_options.copy()
        editor_options['schema'] = schema
        context = {
            "name": name,
            "value": value,
            "options": editor_options,
            "chain_field": self.chain_field
        }
        return mark_safe(renderer.render(self.template_name, context))
