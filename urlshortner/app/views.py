from django.shortcuts import render, redirect, HttpResponse,HttpResponseRedirect
from datetime import datetime
from app.models import Info,JsonEditorModel
from app.forms import JsonEditorForm

# Create your views here.
def index(request):
  try:

    if (request.method == 'POST'):
      yoururl = request.POST.get('url')
      shortnerurl = request.POST.get('shortnerurl')
      p = Info(url = yoururl,shortnerurl = shortnerurl)
      p.save()
      if p.shortnerurl is None:
        mylist=['A', 'a', '1', 'B', 'b', '2', 'C', 'c', '3', 'D', 'd', '4', 'E','e','F','f','G','g','H','7','h','I','i','J','j''K','k',
            'L','l','M','m','N','n','O','o','P','p','Q','q','R','r','S','s','5','T','t','U','u','6','V','v','W','w','8','X',
            'x','Y','9','y','Z','z']
        def alph(n):
          if n<=59:
            mod=n%59
            value=mylist[mod]
            return value
          else:
            value=""
            while n>0:   
                mod=n%59
                value=value+mylist[mod]
                n=n//59
            return value
        q="http://127.0.0.1:8000/"+alph(p.pk)
        p = Info.objects.get(url=yoururl)
        p.shortnerurl=q
        p.save()
        return render(request,'index.html' ,{'x':q})

    return render(request,'index.html')
  except:
    return render(request,'index.html', {'e':'You are entering same url which is already present'})


def search(request):
      try:
        if(request.method == 'POST'):
            shortnerurl = request.POST.get('shortnerurl')
            x = Info.objects.get(shortnerurl = shortnerurl)
            return render(request,'search.html',{'obj':x})
              
        return render(request,'search.html')
      except:
        return render(request,'search.html', {'msg':'you are searching wrong shorturl'})


def msg(request):
  return render(request,"message.html")


def jsoneditor(request):
  if request.method == 'POST':
        form = JsonEditorForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse("data submitted successfully")
        else:
          return render(request,"jsoneditor.html",{'form':form})
  else:
    form=JsonEditorForm()
    return render(request,"jsoneditor.html",{'form':form})
    

def search_view(request,*args,**kwargs):
  short="http://{0}{1}".format(request.get_host(),request.get_full_path())
  longurl=Info.objects.get(shortnerurl=short)
  return redirect(longurl.url)



